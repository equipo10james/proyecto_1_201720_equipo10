Santiago Rangel 201632011
Daniela Gonzalez, 201631227

1. En relacion al uml y al diseno de mundo que planeamos: 
La clase principal es el STSManager que tiene como funcion cargar todos los datos
 guardarlos y realizar funciones con estos datos. Esta clase tiene como atributos
 listas que pueden ser Pila, Cola, RingList, o DoubleLinkedList.(Pila y Cola extienden 
de sus respectivas interfaces) Estas listas son genericas y por lo tanto tienen como
 atributo un Nodo de la clase Node o NodoSimple(si es sencillamente enlazado). Estos 
nodos tiene como atributo un elemento que van a guardar. Este elemento puede ser cualquier
 tipo de VO. Cada uno de estos VO tiene sus propios atributos.

Tuvimos un error con las librerias de google para leer los archivos JSon. No implementamos todos los metodos de lectura
 Json para cada tipo de VO. Implementamos solo los metodos generales que demuestran el conocimiento en relacion a la lectura
 de archivos Json. Posteriormente usaremos este metodo en diferentes implementaciones de acuerdo al VO en cuestion.
2. Orden de complejidad
1A: O(n^2) Como recibe 2 parametros, la informacion se debe sacar de varias listas y luego comparar sobre estas. Esto ya es O(n^2). Tambien que este ordenado implica que en el mejor de los casos sera O(nlongn) usando un buen algoritmo de ordenamiento.
2A: O(n^2) Como recibe 2 parametros, la informacion se debe sacar de varias listas y luego comparar sobre estas. Esto ya es O(n^2). 
3A: O(n^2) El ordenamiento de este meotodo implica que en el mejor de los casos seria nlogn
4A: O(n) Todas las paradas ya estan dadas y no toca comparar. Mediante un metodo recursivo se puede solucionar.
5A: O(n^2) Para cada parada toca recorrer las listas con informacion y anadirlo. Es un doble recorrido
1B: O(n^2) Es un problema analogo al 1A.
2B: O(n^2) Es un problema analogo al 1B.
3B: O(n^2) Segun los dos parametros toca tener como resultado una nueva lista. Esto demoraria (n^2). Luego encasillarlos dentro de una hora no es un problema mayor. 
4B: O(n^3)
5B: O(n) Es un problema analogo al 5A.
1C: O(n) Cargar la informacion simplemente depende de la cantidad de datos en el programa.
2C: O(n) Toca realizar operaciones dentro de una lista para determinar las distancias de un servicio. No toca hacer dobles recorridos para comparar.
3C: O(n^2) Segun los dos parametros toca tener como resultado una nueva lista. Esto demoraria (n^2).
4C: O(n^2) Para una fecha toca sacar unas paradas. Luego hay que hacer un segundo recorrido para determinar cuales se repiten.


SEGUNDA ENTREGA

Requerimientos - Parte A

1A. Para implementar este método lo primero que se hizo fue buscar, en la lista de agencias, la agencia que corresponde al nombre de la empresa ingresado por parámetro. El id de esta empresa se guardó en una variable auxiliar ya que posteriormente se buscaron todas las rutas que tenían el mismo idAgency que el que se guardó. Todas estas rutas se guardaron en una lista auxiliar. Finalmente se buscaron todas las rutas de esta lista auxiliar cuya fecha correspondía con la ingresada por parámetro. Es importante recalcar que se instauró un formato que tiene que cumplir la fecha ingresada por parámetro:
 Se supone que pFecha va a tener un formato AAAAMMDD.Z donde Z es la inicial del dia de la semana en español (i es miercoles).
Todas la rutas que cumplan con la fecha se guardarán en una lista que organizará por idRuta creciente. 

2A. Como se tiene una lista de retrasos y que cada retraso tiene un viaje, una ruta y una fecha, entonces se recorrió esta lista de retrasos y se verificó que coincidiera el id de la ruta del retraso con el idRuta dado por parámetro y de igual manera con la fecha. Se guardaron en una lista todos los viajes de todos los retardos que cumplan con los dos requerimientos. Esta lista se ordenó por idTrip creciente. 

3A. Lo primero que se hace es buscar todos viajes que se realizan en la fecha ingresada por parámetro. Después se guardan todos los retrasos que sean de los viajes que hay en la fecha dada por parámetro. Después se piden todas las paradas de todos los retrasos de todos los viajes en la fecha dada y se agregan a la lista. Finalmente todas estas paradas se agregan a una cola. 

4A. Se utilizó un método recursivo para analizar todos los transbordos posibles para una ruta. Lo primero fue buscar la ruta con el id ingresado por parámetro. Después se recorren las paradas de dicha ruta. Para esto se utilizo un método auxiliar que fue el que implementó la recursividad. Fue muy importante siempre tener clara la ruta de la cual se desprenden los transbordos para no repetir recorridos. El caso que acaba con la recursividad es cuando una parada solo tiene una ruta, es decir la ruta de base ingresada por parámetro. En este caso se integra esa para a la pila y se sale del método. En el caso que la parada tenga mas de una ruta, se recorren las paradas de esas rutas a partir de la parada en la que se encuentran hasta llegar a una en donde ya no haya mas paradas, ni rutas posibles. 

5A.En este método se modificó el api ya que el enunciado dice “Dada una secuencia de paradas”. Este método busca retornar una lista que en cada posición tiene una lista y que esa también tiene una lista de viajes tal que estos viajes cumplan con los requisitos ingresados por parámetro. Lo primero es sacar las paradas ingresadas por parámetro e irlas poniendo en una lista conservando el orden en que fueron dadas. Después se obtienen todos los viajes correspondientes a la fecha ingresada por parámetro. Después se recorren todas las paradas dadas por parámetro y los viajes en la fecha dada. Todos los viajes que cumplan con el rango de hora y cuyo stop id coincida con alguno de los stops id de los stops dados por parámetro, entonces el viaje se agrega a una lista de viajes. Después a una lista se agregan todas estas listas de viajes. Cabe resaltar que este punto estaba muy mal planteado porque en el api y según la descripción en el enunciado pedían una lista de una lista de una lista de viajes. Sin embargo también nos pedían analizar las rutas de las paradas, pero estas paradas no se podían retornar, porque la signatura del método no lo permitía. Por ende se analizó pero solo se retorno la lista de viajes. 

Requerimientos - Parte B


-Agregue el atributo a retraso de cuantos segundos debe tiene de retardo y su parada asociada
-Agregue como a atributo a VOtrip una lista de paradas con retardos y un metodo para definir esa informacion que la ordena por segundos de retardos(No incluida en el constructor)
-Para el 2B, se retorna la lista de trips ordenadas de acuerdo al tripID. Cada uno de los trips tiene su lista de retardos ordenada por tiempo de retardo
-Agregue un booleano a stop para ver si tiene retraso(No incluido en el constructor, debe definirse a la hora de crear los retrasos)
-Agregue el atributo de retrasos en Rango de Hora (Toca hacer el codigo para que se creen estas listas)
-En busupdate puse el metodo que pasa la hora a segundos
-En VOtrip hice un metodo que saca su retardo promedio y su distancia, y para VOroute tambien
-Cree la clase VOservice y un metodo en sts para definir la lista de service que esta como atributo


