Santiago Rangel 201632011
Daniela Gonzalez, 201631227

1. En relación entre el uml y la diseño de mundo que planeamos: 
La clase principal es el STSManager que tiene como función cargar todos los datos,
 guardarlos y realizar funciones con estos datos. Esta clase tiene como atributos
 listas que pueden ser Pila, Cola, RingList, o DoubleLinkedList.(Pila y Cola extienden de sus respectivas interfaces) Estas listas son genéricas y por lo tanto tienen como atributo un Nodo de la clase Node o NodoSimple(si es sencillamente enlazado). Estos nodos tiene como atributo un elemento que van a guardar. Este elemento puede ser cualquier tipo de VO. Cada uno de estos VO tiene sus propios atributos.

Tuvimos un error con las librerias de google para leer los archivos JSon (GSON) al tratar de importar esta desde el Build Path ya que a pesar que agregábamos los JARs esto no parecía tener efecto en los métodos donde se utilizó GSON. En cuanto a la lectura de los dos archivos de este tipo, se implementó el método para la lectura del archivo que actualiza los buses y su estado. Este mismo código y esquema es el que se va a utilizar para leer el otro archivo solo que utilizando <VOStopUpdate> en vez de <VOBusUpdate>.

Para los archivos CSV, se implementaron métodos para la lecturas específica de archivos de rutas, viajes, stoptimes y paradas. Estos son los modelos para leer los archivos CSV como por ejemplo feed_info,.. entre otros. Se utilizó BufferReader ya que todos se tratan de archivos CSV simples donde no hay comas en los datos del archivo. 

Se crearon las signaturas de todos los métodos que se van a usar así como sus clases tal y como figuran en el diagrama de UML disponible en la carpeta de docs. 

2. Orden de complejidad
1A: O(n^2) Como recibe 2 parametros, la informacion se debe sacar de varias listas y luego comparar sobre estas. Esto ya es O(n^2). Tambien que este ordenado implica que en el mejor de los casos sera O(nlongn) usando un buen algoritmo de ordenamiento.
2A: O(n^2) Como recibe 2 parametros, la informacion se debe sacar de varias listas y luego comparar sobre estas. Esto ya es O(n^2). 
3A: O(n^2) El ordenamiento de este meotodo implica que en el mejor de los casos seria nlogn
4A: O(n) Todas las paradas ya estan dadas y no toca comparar. Mediante un metodo recursivo se puede solucionar.
5A: O(n^2) Para cada parada toca recorrer las listas con informacion y anadirlo. Es un doble recorrido
1B: O(n^2) Es un problema analogo al 1A.
2B: O(n^2) Es un problema analogo al 1B.
3B: O(n^2) Segun los dos parametros toca tener como resultado una nueva lista. Esto demoraria (n^2). Luego encasillarlos dentro de una hora no es un problema mayor. 
4B: O(n^3)
5B: O(n) Es un problema analogo al 5A.
1C: O(n) Cargar la informacion simplemente depende de la cantidad de datos en el programa.
2C: O(n) Toca realizar operaciones dentro de una lista para determinar las distancias de un servicio. No toca hacer dobles recorridos para comparar.
3C: O(n^2) Segun los dos parametros toca tener como resultado una nueva lista. Esto demoraria (n^2).
4C: O(n^2) Para una fecha toca sacar unas paradas. Luego hay que hacer un segundo recorrido para determinar cuales se repiten.
