package testListas;

import junit.framework.TestCase;
import model.data_structures.Pila;

public class PilaTest<E> extends TestCase
{
	private Pila pila;
	

	public void setup1()
	{
		pila = new Pila<E>();

	}
	
	public void setup2()
	{
		pila = new Pila<E>();
		Object cosa1 = 1;
		pila.push(cosa1);
		
	}
	
	public void setup3()
	{
		pila = new Pila<E>();
		Object cosa1 = 1;
		Object cosa2 = 2;
		Object cosa3 = 3;
		Object cosa4 = 4;
		Object cosa5 = 5;
		Object cosa6 = 6;
		pila.push(cosa1);
		pila.push(cosa2);
		pila.push(cosa3);
		pila.push(cosa4);
		pila.push(cosa5);
		pila.push(cosa6);
		
	}

	public void testSize()
	{
		setup1();
		assertEquals(0, pila.getSize());
		assertEquals(true, pila.isEmpty());
		
	}
	
	public void testSize1()
	{
		setup2();
		assertEquals(1, pila.getSize());
	}
	public void testSize2()
	{
		setup3();
		assertEquals(6, pila.getSize());
	}

	public void testPush()
	{
		setup2();
		Object cosa = 2;
		pila.push( cosa);
		assertEquals(2, pila.getTop());
		assertEquals(2, pila.getSize());
		
		
	}
	
	public void testPop()
	{
		setup3();
		assertEquals(6, pila.pop());
		assertEquals(5, pila.pop());
		assertEquals(4, pila.pop());
		assertEquals(3, pila.pop());
		assertEquals(2, pila.pop());
		assertEquals(1, pila.pop());
		assertTrue(pila.isEmpty());

	}
	public void testGetTop()
	{
		setup3();
		pila.pop();
		assertEquals(5, pila.getTop());
		pila.pop();
		assertEquals(4, pila.getTop());
	}
}
