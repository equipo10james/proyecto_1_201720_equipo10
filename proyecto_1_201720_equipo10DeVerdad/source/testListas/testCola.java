package testListas;

import junit.framework.TestCase;
import model.data_structures.Cola;
import model.data_structures.NodeSimple;
import model.data_structures.Pila;

public class testCola<E> extends TestCase
{
	private Cola cola;

	
	public void setup1()
	{
		cola = new Cola<E>();

	}
	
	public void setup2()
	{
		cola = new Cola<E>();
		Object cosa1 = 1;
		cola.enqueue(cosa1);
		
	}
	
	public void setup3()
	{
		cola = new Cola<E>();
		Object cosa1 = 1;
		Object cosa2 = 2;
		Object cosa3 = 3;
		Object cosa4 = 4;
		Object cosa5 = 5;
		Object cosa6 = 6;
		cola.enqueue(cosa1);
		cola.enqueue(cosa2);
		cola.enqueue(cosa3);
		cola.enqueue(cosa4);
		cola.enqueue(cosa5);
		cola.enqueue(cosa6);
		
	}

	public void testSize()
	{
		setup1();
		assertEquals(0, cola.getSize());
		assertEquals(true, cola.isEmpty());
		
	}
	
	public void testSize1()
	{
		setup2();
		assertEquals(1, cola.getSize());
	}
	public void testSize2()
	{
		setup3();
		assertEquals(6, cola.getSize());
		cola.dequeue();
		cola.dequeue();
		cola.dequeue();
		assertEquals(3, cola.getSize());
	}
	public void testGetFirst()
	{
		setup3();
		assertEquals(1, cola.getFirst());
		cola.dequeue();
		assertEquals(2, cola.getFirst());
	}
	public void testGetLast()
	{
		setup3();
		Object cosa7 = 7;
		assertEquals(6, cola.getLast());
		cola.enqueue(cosa7);
		assertEquals(7, cola.getLast());
	}

	public void testEnqueue()
	{
		Object cosa7 = 7;
		setup2();
		cola.enqueue(cosa7);
		assertEquals(2, cola.getSize());
		assertEquals(7, cola.getLast());
	}
	
	public void testDequeue()
	{
		setup3();
		Object cosa7 = 7;
		assertEquals(6, cola.getLast());
		cola.enqueue(cosa7);
		assertEquals(7, cola.getLast());
	}
	
	

}
