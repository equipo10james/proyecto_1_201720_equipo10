package api;


import model.data_structures.Cola;
import model.data_structures.Pila;
import model.data_structures.RingList;
import model.vo.RangoHora;
import model.vo.VOAgency;
import model.vo.VORetraso;
import model.vo.VORoute;
import model.vo.VOStop;

import model.vo.VOTrip;
import model.vo.VOservice;


public interface ISTSManager 
{
	public void ITScargarGTFS( );
	
	public void ITScargarTR(); 
	
	public RingList<VORoute> ITSrutasPorEmpresa(String nombreEmpresa, String fecha);
	
	public RingList<VOTrip> ITSviajesRetrasadosRuta(String idRuta, String fecha);
	
	public Cola<VOStop> ITSparadasRetrasadasFecha( String fecha);
	
	public Pila<Pila<VOStop>> ITStransbordosRuta(String idRuta, String fecha); 
	
	public RingList<RingList<RingList<VOTrip>>> ITSrutasPlanUtilizacion(Cola<VOStop> stops, String fecha, String horaInicio,
			String horaFin);
	
	public RingList<VORoute> ITSrutasPorEmpresaParadas(String nombreEmpresa, String fecha);
	
	public RingList<VOTrip> ITSviajesRetrasoTotalRuta(String idRuta, String fecha);
	
	public RingList<RangoHora> ITSretardoHoraRuta (String idRuta, String Fecha );
	
	public RingList <VOTrip> ITSbuscarViajesParadas(String idOrigen, String idDestino, String fecha, String horaInicio, String horaFin); 

	public VORoute ITSrutaMenorRetardo (String idRuta, String fecha);
	
	public void ITSInit();
	
	public RingList<VOservice> ITSserviciosMayorDistancia (String fecha);
	
	public RingList<VORetraso> ITSretardosViaje (String fecha, String idViaje);
	
	public RingList <VOStop> ITSparadasCompartidas (String fecha);
	
}
