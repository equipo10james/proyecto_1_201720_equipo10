package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;


public class Cola<E> implements Iterable, IQueue<E>
{

	private int size;
	private NodeSimple<E> first;
	private NodeSimple<E> last;
	
	public Cola()
	{
		size = 0;
		first = null;
		last = null;
	}
	
	public int getSize()
	{
		return size;
	}
	
	public boolean isEmpty()
	{
		return first == null;
	}
	
	@Override
	public void enqueue(E item)
	{
		NodeSimple nodo = new NodeSimple(item, null);
		if(isEmpty())
		{
			first = nodo;
			last = nodo;
		}
		else
		{
			last.setNext(nodo);
			last = nodo;
		}
		
		size ++;
		
	}

	@Override
	public E dequeue()
	{
		return (E) dequeueNodo().getElement(); //retorna el primer elemento
	}

	public NodeSimple dequeueNodo()
	{
		NodeSimple rta = null;
		if(!isEmpty())
		{
			 rta = first;
			first = first.getNext();		
			size --;
		}
		
		
		return rta; //retorna el primer nodo
	}
	public E getFirst()
	{
		if(!isEmpty())
			{
			return first.getElement();
			}
		else
		{
			return null;

		}
	}
	
	public E getLast()
	{

		if(!isEmpty())
			{
			return last.getElement();
			}
		else
		{
			return null;

		}
	}

	@Override
	public Iterator iterator() 
	{
		return new ListIterator<E>();
	
	}

	
	
	 private class ListIterator<E> implements Iterator<E>
	 {
	        private NodeSimple<E> currentNode;
	        public ListIterator() 
	        {
	            super();
	            this.currentNode = (NodeSimple<E>) first;
	        }
	        @Override
	        public boolean hasNext()
	        {
	            if (currentNode != null && currentNode.getNext() != null)
	                return true;
	            else
	                return false;
	        }
	        @Override
	        public E next()
	        {
	            if (!hasNext())
	                throw new NoSuchElementException();
	            E node = currentNode.getElement();
	            currentNode = currentNode.getNext();
	            return node;
	        }
	        
	        @Override
	        public void remove() 
	        {
	            // TODO Auto-generated method stub
	        	currentNode = null;
	        }
	    }

	
	
}
