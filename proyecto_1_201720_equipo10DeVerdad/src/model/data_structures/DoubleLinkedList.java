package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class DoubleLinkedList <E> implements Iterable
{  
	public static class Node<E>
	{
		private E elemento;
		private Node<E> prev;
		private Node<E> next;
		
		public Node(E e, Node<E> p, Node<E> n)
		{
			elemento = e;
			prev = p;
			next = n;
		}
		
		public E getElement()
		{
			return elemento;
		}
		
		public Node<E> getPrev() 
		{
			return prev;
		}
		
		public Node<E> getNext()
		{
			return next;
		}
		
		public void setPrev(Node<E> p )
		{
			prev = p;
		}
		
		public void setNext(Node<E> n)
		{
			next = n;
		}
		
	}
	
	private Node<E> head;
	private Node<E> tail; 
	private int size=0;
	private Node<E> current;
	
	public DoubleLinkedList()
	{
		head = null;
		tail = null;
		size = 0;
	}
	
	public int size()
	{
		return size;
	}
	public boolean isEmpty()
	{
		return size == 0;
	}
	public E first()
	{
		if(isEmpty())
		{
			return null;
		}
		return head.getElement();
		
	}
	public E last()
	{
		if(isEmpty())
		{
			return null;
		}
		return tail.getElement();
		
	}
	
	public void addFirst(E cosa)
	{
		Node nodo = new Node<E>(cosa, null, null);
	
		if(head==null)
		{
			head= nodo;
			tail= nodo;
		}
		else
		{	
			Node primerNodo = head;
			nodo.setNext(primerNodo);
			primerNodo.setPrev(nodo);
			head =nodo;
		}
		size ++;
	}
	
	public void addLast(E cosa)
	{
		Node nodo = new Node<E>(cosa, null, null);
		
		if(head==null || tail == null)
		{
			tail= nodo;
			head = nodo;
		}
		else
		{	
			tail.setNext(nodo);
			nodo.setPrev(tail);
			tail = nodo;
		}
		size ++;
	}
	
	public void removeFirst()
	{
		head.next.setPrev(null);
		head = head.next;
		size --;
	}

	public void removeLast()                                          
	{
		tail.prev.setNext(null);
		tail = tail.prev;
		size --;
	}
	public Node getNode(int i)
	{
        Node p = head;
        int contador = 1;
        while(contador != i)
        {
        	p = p.getNext();
        	contador ++;
        	if (contador == i)
        	{
        		break;
        	}
        }
        
		return p;
        
    }
	
	public void addPos(E cosa, int pos)
	{
		Node nuevo = new Node<E>(cosa, null, null);
		Node nodoPos = getNode(pos);
		nuevo.setNext(nodoPos);
		nuevo.setPrev(nodoPos.getPrev());
		nodoPos.prev.setNext(nuevo);
		nodoPos.setPrev(nuevo);
		size ++;
	}
	public void removePos(int pos)
	{
		if(pos ==1)
		{
			removeFirst();
		}
		else if(pos == size)
		{
			removeLast();
		}
		else
		{
			Node nodoPos = getNode(pos);
			nodoPos.prev.setNext(nodoPos.getNext());
			nodoPos.next.setPrev(nodoPos.getPrev());
			size --;
		}
	
	}

	public void previous() 
	{
		current = current.next;
	}


	public void next() 
	{
		current = current.prev;
	}


	public Object getCurrentElement() 
	{
		return current.getElement();
	}


	public Object getElement(int pos)
	{
		return getNode(pos).getElement();
	}

	public void delete() 
	{
		//el actual
		current.prev.setNext(current.next);
		current.next.setPrev(current.prev);
	}

	public E buscar(E elemento) {
		 E rta = null;

		Iterator<E> iter = this.iterator();
		while (iter.hasNext() && rta == null) 
		{
			E currentElement = iter.next();
			
			if( elemento.equals(currentElement))
			{
				rta = currentElement;
			}
		}

		return rta;
    }
	
	public void switchWithNext(int pos)
	{
		Node nodo = getNode(pos);
		Node nodo2 = getNode(pos+1);
		
		
	}
	@Override
	public Iterator iterator() {
		// TODO Auto-generated method stub
		return new ListIterator<E>();
	}

	 private class ListIterator<E> implements Iterator<E>
	 {
	        private Node<E> currentNode;
	        public ListIterator() 
	        {
	            super();
	            this.currentNode = (Node<E>) head;
	        }
	        @Override
	        public boolean hasNext()
	        {
	            if (currentNode != null && currentNode.next != null)
	                return true;
	            else
	                return false;
	        }
	        @Override
	        public E next()
	        {
	            if (!hasNext())
	                throw new NoSuchElementException();
	            E node = currentNode.elemento;
	            currentNode = currentNode.next;
	            return node;
	        }
	        
	        @Override
	        public void remove() 
	        {
	            // TODO Auto-generated method stub
	        	currentNode = null;
	        }
	    }

	
	
}
