package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

import model.data_structures.NodeSimple;
public class Pila<E> implements IStack<E>, Iterable

{
	
	private int size;
	private NodeSimple head;
	
	
	public Pila()
	{
		head = null;
		size = 0;
	}
	
	public int getSize()
	{
		return size;
	}
	
	public boolean isEmpty()
	{
		return head == null;				
	}
	

	@Override
	public void push(E item) 
	{
		NodeSimple<E> nuevo = new NodeSimple<E>(item, head);
		head = nuevo;
		size ++;
		
	}

	@Override
	public E pop() 
	{	
		
		E rta = (E) head.getElement();
		if(head!=null)
		{
			head = head.getNext();	
			size --;
		}
		
		return rta;
	}
	
	public E getTop()
	{
		return (E) head.getElement();
	}

	@Override
	public Iterator iterator() 
	{
		return new ListIterator<E>();
	
	}

	 private class ListIterator<E> implements Iterator<E>
	 {
	        private NodeSimple<E> currentNode;
	        public ListIterator() 
	        {
	            super();
	            this.currentNode = (NodeSimple<E>) head;
	        }
	        @Override
	        public boolean hasNext()
	        {
	            if (currentNode != null && currentNode.getNext() != null)
	                return true;
	            else
	                return false;
	        }
	        @Override
	        public E next()
	        {
	            if (!hasNext())
	                throw new NoSuchElementException();
	            E node = currentNode.getElement();
	            currentNode = currentNode.getNext();
	            return node;
	        }
	        
	        @Override
	        public void remove() 
	        {
	            // TODO Auto-generated method stub
	        	currentNode = null;
	        }
	    }


	




	

	

}
