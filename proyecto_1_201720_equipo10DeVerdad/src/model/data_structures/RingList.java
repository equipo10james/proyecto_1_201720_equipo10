package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;
import model.data_structures.DoubleLinkedList.Node;

public class RingList <E> implements Iterable
{
	
	
	private Node<E> head;
	private int size=0;
	private Node<E> current;
	
	public RingList()
	{
		head = null;
		size = 0;
	}
	public int size()
	{
		return size;
	}
	public boolean isEmpty()
	{
		return size == 0;
	}
	
	public E first()
	{
		if(isEmpty())
		{
			return null;
		}
		return head.getElement();
		
	}
	
	
	
	public void addFirst(E cosa)
	{
		Node nodo = new Node<E>(cosa, null, null);
		if(head == null)
		{
			head=nodo;
			head.setNext(head);
			head.setPrev(head);
			
		}
		else
		{
			nodo.setNext(head);
			nodo.setPrev(head.getPrev());
			head.setPrev(nodo);
			nodo.getPrev().setNext(nodo);
			head = nodo;
		}
		size ++;
	}

	public void addLast(E cosa)
	{
		Node nodo = new Node<E>(cosa, null, null);
		if(head == null)
		{
			head=nodo;
			head.setNext(head);
			head.setPrev(head);
			
		}
		else
		{
			nodo.setNext(head);
			nodo.setPrev(head.getPrev());
			head.getPrev().setNext(nodo);
			head.setPrev(nodo);
			
		}
		size ++;
	}
    
	public void removeFirst()
	{
		
		if(head != null)
		{
			if(head.getNext() == head)
			{
				head = null;
			}
			else
			{
				head.getNext().setPrev(head.getPrev());
				head.getPrev().setNext(head.getNext());
				head = head.getNext();
			}
			size --;
		}
	
		
	}
	public void removeLast()
	{
		
		if(head != null)
		{
			if(head.getNext() == head)
			{
				removeFirst();
			}
			else
			{
				head.getPrev().getPrev().setNext(head);
				head.setPrev(head.getPrev().getPrev());
				size --;
			}
			
		}
	
		
	}
	public Node getNode(int i)
	{
		if(i > size)
		{
			i = i % size;
		}
        Node p = head;
        int contador = 1;
        while(contador < i)
        {
        	p = p.getNext();
        	contador ++;
        }
        
		return p;
        
    }

	public void addPos(E cosa, int pos)
	{
		if(pos == 1)
		{
			addFirst(cosa);
		}
		else
		{
			Node nuevo = new Node<E>(cosa, null, null);
			Node nodoPos = getNode(pos);
			nuevo.setNext(nodoPos);
			nuevo.setPrev(nodoPos.getPrev());
			nodoPos.getPrev().setNext(nuevo);
			nodoPos.setPrev(nuevo);
			
		}
		size ++;
		
	}

	public void removePos( int pos)
	{
		if (pos == size)
		{
			removeLast();
		}
		else if(pos == 1)
		{
			removeFirst();
		}
		else
		{
			Node nodoPos = getNode(pos);
			nodoPos.getPrev().setNext(nodoPos.getNext());
			nodoPos.getNext().setPrev(nodoPos.getPrev());
			size --;	
		}
		
	}
	public void previous() 
	{
		current = current.getNext();
	}


	public void next() 
	{
		current = current.getPrev();
	}


	public Object getCurrentElement() 
	{
		return current.getElement();
	}


	public Object getElement(int pos)
	{
		return getNode(pos).getElement();
	}

	public void delete() 
	{
		//el actual
		current.getPrev().setNext(current.getNext());
		current.getNext().setPrev(current.getPrev());
	}
	
	public E buscar(E elemento) {
		 E rta = null;

		Iterator<E> iter = this.iterator();
		while (iter.hasNext() && rta == null) 
		{
			E currentElement = iter.next();
			
			if( elemento.equals(currentElement))
			{
				rta = currentElement;
			}
		}

		return rta;
   }
	
	public void switchWithNext(int pos)
	{
		Node a = getNode(pos-1);
		Node b = getNode(pos+2);
		Node nodo = getNode(pos);
		Node nodo2 = getNode(pos+1);
		a.setNext(nodo2);
		nodo2.setNext(nodo);
		nodo.setNext(b);
		b.setPrev(nodo);
		nodo.setPrev(nodo2);
		nodo2.setPrev(a);
		
	}
	@Override
	public Iterator iterator() {
		// TODO Auto-generated method stub
		return new ListIterator<E>();
	}

	
	
	private class ListIterator<E> implements Iterator<E>
	 {
	        private Node<E> currentNode;
	        public ListIterator() 
	        {
	            super();
	            this.currentNode = (Node<E>) head;
	        }
	        @Override
	        public boolean hasNext()
	        {
	            if (currentNode != null && currentNode.getNext() != null)
	                return true;
	            else
	                return false;
	        }
	        @Override
	        public E next()
	        {
	            if (!hasNext())
	                throw new NoSuchElementException();
	            E node = currentNode.getElement();
	            currentNode = currentNode.getNext();
	            return node;
	        }
	        
	        @Override
	        public void remove() 
	        {
	            // TODO Auto-generated method stub
	        	currentNode = null;
	        }
	    }
	
}
