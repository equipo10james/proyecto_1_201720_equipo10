package model.logic;

import java.awt.ItemSelectable;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Iterator;

import com.google.gson.Gson;

import api.ISTSManager;
import model.data_structures.Cola;
import model.data_structures.DoubleLinkedList;
import model.data_structures.DoubleLinkedList.Node;
import model.data_structures.Pila;
import model.data_structures.RingList;
import model.vo.RangoHora;
import model.vo.VOAgency;
import model.vo.VOBusUpdate;
import model.vo.VOCalendar;
import model.vo.VOCalendarDates;
import model.vo.VOFeedInfo;
import model.vo.VORetraso;
import model.vo.VORoute;
import model.vo.VOShape;
import model.vo.VOStop;
import model.vo.VOStopTimes;
import model.vo.VOStopUpdate;
import model.vo.VOTransfers;
import model.vo.VOTrip;
import model.vo.VOZone;
import model.vo.VOservice;

public class STSManager implements ISTSManager 
{

	private RingList<VORoute> listaRutas;

	private DoubleLinkedList<VOStopTimes> listaStopTimes;

	private RingList<VOTrip> listaViajes;

	private RingList<VOStop> listaParadas;

	private RingList<VOZone> listaZonas;

	private Cola<VOBusUpdate> actualizacionBus;

	private Cola<VOStopUpdate> actualizacionesParadas;

	private RingList<VOAgency> listaAgencias;

	private RingList<VOCalendar> listaCalendario;

	private RingList<VOCalendarDates> listaExcepcionesCalendario;

	private RingList<VOFeedInfo> listaFeed;

	private RingList<VORetraso> listaRetrasos;

	private RingList<VOShape> listaShapes;

	private RingList<VOTransfers> listaTransfers;

	private RingList<VOservice> listaServices;
	
	public STSManager()
	{
		this.ITSInit();
	}

	public void readBusUpdate(File rtFile) 
	{
		actualizacionBus = new Cola<VOBusUpdate>();
		Gson gson = new Gson();

		try
		{
			VOBusUpdate[] info = gson.fromJson(new FileReader(rtFile), VOBusUpdate[].class);
			for (int i=0; i<info.length; i++ )
			{
				actualizacionBus.enqueue(info[i]);
			}
		}

		catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	public void readStopUpdate(File stFile) 
	{
		actualizacionesParadas = new Cola<VOStopUpdate>();
		Gson gson = new Gson();

		try
		{
			VOStopUpdate[] info = gson.fromJson(new FileReader(stFile), VOStopUpdate[].class);
			for (int i=0; i<info.length; i++ )
			{
				actualizacionesParadas.enqueue(info[i]);
			}
		}

		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void loadRoutes(String routesFile) 
	{
		try 
		{
			listaRutas = new RingList<VORoute>();

			BufferedReader br = new BufferedReader(new FileReader(routesFile));
			String linea = br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");

				VORoute ruta = new VORoute (info[0], info[1], info[2], info[3], info[4], info[5], info[6], info[7], info[8]);
				listaRutas.addLast(ruta);


				linea = br.readLine();


				System.out.println(ruta);
			}

			br.close();
		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 

	}


	public void loadTrips(String tripsFile) 
	{
		try 
		{
			listaViajes = new RingList<VOTrip>();

			BufferedReader br = new BufferedReader(new FileReader(tripsFile));
			String linea = br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");

				VOTrip viaje = new VOTrip (info[0], info[1], info[2], info[3], info[4], info[5], info[6], info[7], info[8], info[9]);
				listaViajes.addLast(viaje);

				linea = br.readLine();
			}
			br.close();


			for (int i = 0; i < listaRutas.size(); i++) 
			{
				for (int j = 0; j < listaViajes.size(); j++) 
				{
					if( ((VOTrip) listaViajes.getElement(j)).route_id().equals(((VORoute) listaRutas.getElement(i)).id()))
					{
						((VORoute) listaRutas.getElement(i)).agregarViaje( (VOTrip) listaViajes.getElement(j) );
					}
				}
			}

		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 
	}


	public void loadStopTimes(String stopTimesFile) 
	{

		try 
		{
			listaStopTimes = new DoubleLinkedList<VOStopTimes>();

			BufferedReader br = new BufferedReader(new FileReader(stopTimesFile));
			String linea = br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");

				VOStopTimes stopTime = new VOStopTimes (info[0], info[1], info[2], info[3], info[4], info[5], info[6], info[7], info[8]);
				listaStopTimes.addLast(stopTime);

				linea = br.readLine();
			}

			br.close();
		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 
	}


	public void loadStops(String stopsFile) 
	{

		try 
		{
			listaParadas = new RingList<VOStop>();
			listaZonas = new RingList<VOZone>();

			BufferedReader br = new BufferedReader(new FileReader(stopsFile));
			String linea = br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");

				VOStop parada = new VOStop (info[0], info[1], info[2], info[3], info[4], info[5], info[6], info[7], info[8], info[9]);
				listaParadas.addLast(parada);

				linea = br.readLine();
			}

			br.close();

			for (int i = 0; i < listaParadas.size(); i++) 
			{
				boolean sePuedeAgregar = false;

				for (int j = 0; j < listaZonas.size(); j++) 
				{
					if ( !(((VOStop) listaParadas.getElement(i)).zone_id().equals(((VOZone) listaZonas.getElement(j)).zone_id())))
					{
						sePuedeAgregar = true;
					}

					else
					{
						((VOZone) listaZonas.getElement(j)).agregarParada(((VOStop) listaParadas.getElement(i)));
					}
				}

				if (sePuedeAgregar)
				{
					VOZone zona = new VOZone ( ((VOStop) listaParadas.getElement(i)).zone_id() , (VOStop) listaParadas.getElement(i));
					listaZonas.addFirst(zona);
				}

			}

		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 

	}

	public void loadAgency(String agencyFiles)
	{
		try 
		{
			listaAgencias = new RingList<VOAgency>();

			BufferedReader br = new BufferedReader(new FileReader(agencyFiles));
			String linea = br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");

				VOAgency agencia = new VOAgency (info[0], info[1], info[2], info[3], info[4]);
				listaAgencias.addLast(agencia);


				linea = br.readLine();
			}

			br.close();
		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 
	}

	public void loadCalendar(String calendarFiles)
	{
		try 
		{
			listaCalendario = new RingList<VOCalendar>();

			BufferedReader br = new BufferedReader(new FileReader(calendarFiles));
			String linea = br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");

				VOCalendar calendario = new VOCalendar (info[0], info[1], info[2], info[3], info[4], info[5], info[6], info[7], info[8], info [9]);
				listaCalendario.addLast(calendario);


				linea = br.readLine();
			}

			br.close();
		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 
	}

	public void loadCalendar_dates(String calendar_datesFiles)
	{
		try 
		{
			listaExcepcionesCalendario = new RingList<VOCalendarDates>();

			BufferedReader br = new BufferedReader(new FileReader(calendar_datesFiles));
			String linea = br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");

				VOCalendarDates agencia = new VOCalendarDates (info[0], info[1], info[2]);
				listaExcepcionesCalendario.addLast(agencia);


				linea = br.readLine();
			}

			br.close();
		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 
	}

	public void loadShapes(String shapesFiles)
	{
		try 
		{
			listaShapes = new RingList<VOShape>();

			BufferedReader br = new BufferedReader(new FileReader(shapesFiles));
			String linea = br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");

				VOShape shape = new VOShape (info[0], info[1], info[2], info[3], info[4]);
				listaShapes.addLast(shape);


				linea = br.readLine();
			}

			for (int i = 0; i < listaViajes.size(); i++) 
			{
				for (int j = 0; j < listaShapes.size(); j++) 
				{
					if(  ((VOTrip) listaViajes.getElement(i)).shape_id().equals(  ((VOShape) listaShapes.getElement(j)).shape_id() ))
					{
						((VOTrip) listaViajes.getElement(i)).agregarShape(((VOShape) listaShapes.getElement(j)));
					}
				}
			}


			br.close();
		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 
	}

	public void loadFeedInfo(String feed_infoFiles)
	{
		try 
		{
			listaFeed = new RingList<VOFeedInfo>();

			BufferedReader br = new BufferedReader(new FileReader(feed_infoFiles));
			String linea = br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");

				VOFeedInfo feed = new VOFeedInfo (info[0], info[1], info[2], info[3], info[4], info[5]);
				listaFeed.addLast(feed);


				linea = br.readLine();
			}

			br.close();
		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 
	}

	public void loadTransfers(String transfersFiles)
	{
		try 
		{
			listaTransfers = new RingList<VOTransfers>();

			BufferedReader br = new BufferedReader(new FileReader(transfersFiles));
			String linea = br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");

				VOTransfers transfer = new VOTransfers (info[0], info[1], info[2], info[3]);
				listaTransfers.addLast(transfer);


				linea = br.readLine();
			}

			br.close();
		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 
	}

	public void cargarInfoTrip(VOTrip viaje)
	{

		for (int i = 0; i < listaStopTimes.size(); i++) 
		{

			VOStopTimes actual = ((VOStopTimes) listaStopTimes.getElement(i));

			if(actual.trip_id().equals(viaje.trip_id()))
			{
				viaje.agregarStopTimes(actual);


				for (int j = 0; j < listaParadas.size(); j++) 
				{
					if(((VOStop) listaParadas.getElement(j)).id().equals(actual.stop_id()))
					{
						viaje.agregarParada(((VOStop) listaParadas.getElement(j)));
					}
				}
			}
		}
	}

	public RingList<VOBusUpdate> darInfoDeBusViaje(String tripId)
	{
		RingList<VOBusUpdate> actualizacionDeBus = new RingList<VOBusUpdate>();

		while(actualizacionBus.iterator().hasNext())
		{
			VOBusUpdate cosa = (VOBusUpdate) actualizacionBus.iterator().next();
			if(cosa.trip_id().equals(tripId))
			{
				actualizacionDeBus.addFirst(cosa);
			}
		}

		return actualizacionDeBus;

	}

	public void cargarRetrasos()
	{

		for (int i = 0; i < listaViajes.size(); i++) 
		{
			encontrarRetardos((VOTrip) listaViajes.getElement(i));
		}
	}

	public void encontrarRetardos(VOTrip viaje)
	{
		cargarInfoTrip(viaje);

		//Buscar actualizaciones del bus para el viaje ingresado por parametro
		RingList<VOBusUpdate> actualizaciones = darInfoDeBusViaje(viaje.trip_id());

		//Lista de los retrasos de viaje
		RingList<VORetraso> retrasos  = new RingList<VORetraso>();

		RingList<VOStop> paradasViaje = viaje.darParadas();
		RingList<VOStopTimes> stopTimesViaje = viaje.darStopTimes();

		Iterator<VOStop> iterador1 = viaje.darParadas().iterator();
		Iterator <VOStopTimes> iterador2 = viaje.darStopTimes().iterator();

		VORoute ruta = null;

		boolean encontre = false;
		for (int i = 0; i < listaRutas.size() && !encontre; i++) 
		{
			if(((VORoute) listaRutas.getElement(i)).id().equals(viaje.route_id()))
			{
				ruta = ((VORoute) listaRutas.getElement(i));

				encontre = true;
			}
		}


		while(iterador1.hasNext()) 
		{
			VOStop stopActual = iterador1.next();
			VOStopTimes stopTimeActual = iterador2.next();


			double lat = Double.parseDouble(stopActual.lat());
			double lon = Double.parseDouble(stopActual.lon());

			boolean seEncontro = false;

			VOBusUpdate menorCercano = null;
			VOBusUpdate mMenorCercano = null;

			double menor = Double.MAX_VALUE;
			double mMenor = Double.MAX_VALUE;

			Iterator <VOBusUpdate> iterador3 = actualizaciones.iterator();
			while(iterador3.hasNext())
			{
				VOBusUpdate actual = iterador3.next();
				double latComp = Double.parseDouble(actual.latitude());
				double lonComp = Double.parseDouble(actual.longitude());
				double distancia = getDistance(lat, lon, latComp, lonComp);

				if(distancia<mMenor)
				{
					menorCercano = mMenorCercano;
					menor = mMenor;
					mMenorCercano = actual;
					mMenor = distancia;
				}

				else if (distancia<menor)
				{
					menorCercano = actual;
					menor = distancia;
				}
			}

			double retardo = 0;

			if(menorCercano != null && mMenorCercano!= null)
			{

				String horaBus1 = "";

				String[] pro1 =  menorCercano.recordedTime().split(" ");
				String[] pro2 = pro1[0].split(":");

				if(pro2[0].startsWith("0"))
					pro2[0] = pro2[0].substring(1);

				if (pro1[1].equals("am"))
				{
					horaBus1 = pro2[0] + ":" + pro2[1] + ":" + pro2[2];
				}

				else if (pro1[1].equals("pm"))
				{
					int h = Integer.parseInt(pro2[0]);
					horaBus1 = h+12 + ":" + pro2[1] + ":" + pro2[2];
				}

				String horaBus2 = "";

				String[] pro3 =  mMenorCercano.recordedTime().split(" ");
				String[] pro4 = pro3[0].split(":");

				if(pro4[0].startsWith("0"))
					pro4[0] = pro3[0].substring(1);

				if (pro3[1].equals("am"))
				{
					horaBus2 = pro4[0] + ":" + pro4[1] + ":" + pro4[2];
				}

				else if (pro3[1].equals("pm"))
				{
					int h = Integer.parseInt(pro4[0]);
					horaBus2 = h+12 + ":" + pro4[1] + ":" + pro4[2];
				}

				int hora1 = horaAsegundos(horaBus1);
				int hora2 = horaAsegundos(horaBus2);

				int promedio = (hora1 + hora2)/2;

				int horaStop = horaAsegundos(stopTimeActual.departure_time());

				retardo = horaStop-promedio;

			}



			if(retardo < 0)
			{
				VORetraso retraso = new VORetraso(stopActual, viaje, ruta, viaje.darCalendar().start_date(), retardo);
				retrasos.addLast(retraso);
			}


		}


		viaje.cargarRetrasos(retrasos);
	}



	public double getDistance(double lat1, double lon1, double lat2, double lon2)
	{
		final double R = 6371*1000; // Radious of the earth
		double latDistance = toRad(lat2-lat1);
		double lonDistance = toRad(lon2-lon1);
		double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		double distance = R * c;

		return distance;

	}

	private double toRad( double value)
	{
		return (value * Math.PI / 180);
	}

	public int horaAsegundos(String hora)
	{
		int rta = 0;
		String[] arreglo= hora.split(":");
		String[] segundosAMPM = arreglo[2].split(" ");
		rta += ((3600* Integer.parseInt(arreglo[0]))+(60*Integer.parseInt(arreglo[1]))+(Integer.parseInt(segundosAMPM[0])));
		if(segundosAMPM[1].equalsIgnoreCase("pm"))
		{
			rta += (12*3600);
		}

		return rta;
	}

	@Override
	public void ITScargarGTFS() 
	{
		loadRoutes("proyecto_1_201720_equipo10/data/routes.txt");
		loadTrips("proyecto_1_201720_equipo10/data/trips.txt");
		loadStopTimes("proyecto_1_201720_equipo10/data/stop_times.txt");
		loadStops("proyecto_1_201720_equipo10/data/stops.txt");
		loadAgency("pproyecto_1_201720_equipo10/data/agency.txt");
		loadCalendar("proyecto_1_201720_equipo10/data/calendar.txt");
		loadCalendar_dates("proyecto_1_201720_equipo10/data/calendar_dates.txt");
		loadShapes("proyecto_1_201720_equipo10/data/shapes.txt");
		loadFeedInfo("proyecto_1_201720_equipo10/data/feed_info.txt");
		loadTransfers("proyecto_1_201720_equipo10/data/transfers.txt");

	}


	@Override
	public void ITScargarTR() 
	{
		File archivo1 = new File("proyecto_1_201720_equipo10/data/RealTime-8-21-BUSES_SERVICE");
		File[] files1 = archivo1.listFiles();
		for (int i = 0; i < files1.length; i++) 
		{
			readBusUpdate(files1[i]);
		}
		File archivo2 = new File("proyecto_1_201720_equipo10/data/RealTime-8-21-STOPS_ESTIM_SERVICES");
		File[] files2 = archivo2.listFiles();
		for (int i = 0; i < files2.length; i++) 
		{
			readStopUpdate(files2[i]);
		}		
		File archivo3 = new File("proyecto_1_201720_equipo10/data/RealTime-8-22-BUSES_SERVICE");
		File[] files3 = archivo3.listFiles();
		for (int i = 0; i < files3.length; i++) 
		{
			readBusUpdate(files3[i]);
		}
		File archivo4 = new File("proyecto_1_201720_equipo10/data/RealTime-8-22-STOPS_ESTIM_SERVICES");
		File[] files4 = archivo4.listFiles();
		for (int i = 0; i < files4.length; i++) 
		{
			readStopUpdate(files4[i]);
		}		

	}


	@Override
	public RingList<VORoute> ITSrutasPorEmpresa(String nombreEmpresa, String fecha) 
	{
		String idEmpresa = new String();
		RingList<VORoute> listaRutasEmpresa = new RingList<VORoute>();
		RingList<VORoute> resp = new RingList<VORoute>();


		for (int i = 0; i < listaAgencias.size(); i++) 
		{
			if (((VOAgency) listaAgencias.getElement(i)).agency_name().equals(nombreEmpresa))
			{
				idEmpresa = ((VOAgency) listaAgencias.getElement(i)).agency_id();
			}
		}

		for (int i = 0; i < listaRutas.size(); i++) 
		{
			if (((VORoute) listaRutas.getElement(i)).agencia_id().equals(idEmpresa))
			{
				listaRutasEmpresa.addFirst(((VORoute) listaRutas.getElement(i)));
			}
		}

		for (int i = 0; i < listaRutasEmpresa.size(); i++) 
		{
			if (((VORoute) listaRutasEmpresa.getElement(i)).verificarFecha(fecha) == true)
			{
				resp.addFirst(((VORoute) listaRutasEmpresa.getElement(i)));
			}
		}

		for (int i = resp.size(); i >= 0; i--)
		{
			for (int j = 0 ; j < resp.size() -1; j++)
			{
				VORoute ruta1 = ((VORoute)resp.getElement(j));
				VORoute ruta2 = ((VORoute)resp.getElement(j+1));;
				if(ruta1.id().compareTo(ruta2.id()) > 0)
				{
					resp.switchWithNext(j);
				}
			}
		}

		return resp;
	}


	@Override
	public RingList<VOTrip> ITSviajesRetrasadosRuta(String idRuta, String fecha) 
	{

		RingList<VOTrip> viajesRetraso = new RingList<VOTrip>();

		for (int i = 0; i < listaRetrasos.size(); i++) 
		{
			if (((VORoute)((VORetraso) listaRetrasos.getElement(i)).ruta()).id().equals(idRuta) && ((VORetraso) listaRetrasos.getElement(i)).darFecha().equals(fecha))
			{
				viajesRetraso.addFirst(((VORetraso) listaRetrasos.getElement(i)).viaje());
			}
		}

		for (int i = viajesRetraso.size(); i >= 0; i--)
		{
			for (int j = 0 ; j < viajesRetraso.size() -1; j++)
			{
				VOTrip viaje1 = ((VOTrip)viajesRetraso.getElement(j));
				VOTrip viaje2 = ((VOTrip)viajesRetraso.getElement(j+1));;
				if(viaje1.trip_id().compareTo(viaje2.trip_id()) > 0)
				{
					viajesRetraso.switchWithNext(j);
				}
			}
		}

		return viajesRetraso;
	}


	@Override
	public Cola<VOStop> ITSparadasRetrasadasFecha(String date) 
	{
		Cola<VOStop> paradasConRetraso = new Cola<VOStop>();
		RingList<VOStop> temp= new RingList<VOStop>();


		RingList<VOTrip> viajesFecha = darViajesFecha(date);


		Iterator <VOTrip> iterator = viajesFecha.iterator();
		RingList<VORetraso> retrasosViajes = new RingList<VORetraso>();

		while(iterator.hasNext())
		{
			VOTrip actual = iterator.next();

			for (int i = 0; i < listaRetrasos.size(); i++) 
			{
				if (((VORetraso) listaRetrasos.getElement(i)).viaje().equals(actual))
				{
					retrasosViajes.addFirst(((VORetraso) listaRetrasos.getElement(i)));;
				}
			}

			Iterator <VORetraso> iterator1= retrasosViajes.iterator();


			while(iterator1.hasNext())
			{

				VORetraso retraso = iterator1.next();

				VOStop laParadaRetrasada = retraso.parada();

				temp.addLast(laParadaRetrasada);

			}
		}

		Iterator<VOStop> iterator2= temp.iterator();

		while(iterator2.hasNext())
		{

			VOStop parada = iterator2.next();
			paradasConRetraso.enqueue(parada);
		}

		return paradasConRetraso;
	}


	@Override
	public Pila<Pila<VOStop>> ITStransbordosRuta(String idRuta, String fecha) 
	{
		//Guardar la ruta que tiene por id el ingresado por parametro
		VORoute ruta = null;

		for (int i = 0; i < listaRutas.size(); i++) 
		{
			if(((VORoute) listaRutas.getElement(i)).id().equals(idRuta))
			{
				ruta = ((VORoute) listaRutas.getElement(i));
			}
		}

		Pila<Pila<VOStop>> resp = new Pila<Pila<VOStop>>();

		for (int i = 0; i < ruta.darParadas().size(); i++) 
		{
			resp.push(recur( ((VOStop) ruta.darParadas().getElement(i)), ruta));
		}


		return resp;
	}

	private Pila<VOStop> recur(VOStop parada, VORoute ruta)
	{
		Pila<VOStop> trasbordos = new Pila<VOStop>();

		//Caso que sale de la recursion: si por la parada solo pasa la ruta entrada por patametro
		if(parada.darRutas().size() == 1)
		{
			trasbordos.push(parada);
		}

		//Recursión: en el caso en el que por la parada pasen mas de una ruta
		else
		{
			//Agrega la parada de la que se desprenden los transbordos
			trasbordos.push(parada);

			for (int i = 0; i < parada.darRutas().size(); i++) 
			{
				VORoute rutaActual = (VORoute) parada.darRutas().getElement(i);

				if(!rutaActual.equals(ruta))
				{

					//Tengo que recorrer las paradas que van despues de parada para cada nueva ruta
					RingList<VOStop> paradasActual = rutaActual.darParadas();
					boolean llegoParada = false;

					for (int j = 0; j < paradasActual.size(); j++) 
					{
						if(paradasActual.getElement(i).equals(parada) &&!llegoParada)
						{
							llegoParada = true;
						}

						if(llegoParada == true && !paradasActual.getElement(i).equals(parada))
						{
							recur(((VOStop) paradasActual.getElement(i)), rutaActual);
						}
					}

				}
			}

		}


		return trasbordos;
	}

	public void cargarRutasQuePasanParada()
	{
		for (int i = 0; i < listaRutas.size(); i++) 
		{
			VORoute rutaActual = (VORoute) listaRutas.getElement(i);

			for (int j = 0; j < rutaActual.darParadas().size(); j++) 
			{
				for (int j2 = 0; j2 < listaParadas.size(); j2++) 
				{
					if(((VOStop) rutaActual.darParadas().getElement(j)).id().equals(((VOStop) listaParadas.getElement(j2)).id()) )
					{
						((VOStop) listaParadas.getElement(j2)).agregarRuta(rutaActual);
					}
				}
			}
		}
	}

	public RingList<VOTrip> darViajesFecha(String fecha)
	{
		RingList<VOTrip> temps = new RingList<VOTrip>();

		for (int i = 0; i < listaViajes.size(); i++) 
		{
			if(((VOTrip) listaViajes.getElement(i)).verificarFecha(fecha) == true)
			{
				temps.addFirst((VOTrip) listaViajes.getElement(i));
			}
		}

		return temps;
	}

	@Override
	public RingList<RingList<RingList<VOTrip>>> ITSrutasPlanUtilizacion(Cola<VOStop> stops, String fecha, String horaInicio,
			String horaFin) 
	{
		RingList<VOStop> temp = new RingList<VOStop>();
		VOStop primero = stops.dequeue();
		temp.addLast(primero);

		for (int i = 0; i < temp.size(); i++) 
		{
			VOStop actual = stops.dequeue();
			temp.addLast(actual);
		}


		RingList<VOTrip> viajesEnFecha = darViajesFecha(fecha); 
		RingList<RingList<RingList<VOTrip>>> stopsUtilizados = new RingList<RingList<RingList<VOTrip>>>();

		Iterator<VOTrip> iterator = viajesEnFecha.iterator();
		Iterator<VOStop> iterator2 = temp.iterator();

		while(iterator2.hasNext())
		{

			VOStop paradaActual = iterator2.next();

			RingList<RingList<VOTrip>> laLista = new RingList<RingList<VOTrip>>();

			RingList<VOTrip> viajesPorParada = new RingList< VOTrip>();
			RingList<VORoute> rutasPorParada = new RingList< VORoute>();

			while(iterator.hasNext())
			{

				VOTrip viajeActual = iterator.next();

				int length = viajeActual.darParadas().size();

				for (int i = 0; i < length; i++) 
				{

					VOStop stop = (VOStop) viajeActual.darParadas().getElement(i);
					VOStopTimes stopTime = (VOStopTimes) viajeActual.darStopTimes().getElement(i);
					int tiempoLlegada = Integer.parseInt(stopTime.arrival_time());

					int pHoraInicio = Integer.parseInt(horaInicio);
					int pHoraFinal = Integer.parseInt(horaFin);

					if(stop.id() == paradaActual.id() &&  tiempoLlegada >= pHoraInicio && tiempoLlegada <= pHoraFinal)
					{

						viajesPorParada.addLast(viajeActual);


						VORoute ruta = null;
						String rutaID = null;
						boolean encontro = false;
						for (int j = 0; j < listaViajes.size() && !encontro; j++) 
						{
							if(((VOTrip) listaViajes.getElement(i)).route_id().equals(viajeActual.route_id()))
							{
								rutaID = ((VOTrip) listaViajes.getElement(i)).route_id();
								encontro = true;
							}
						}

						encontro = false;

						for (int j = 0; j < listaRutas.size() && !encontro; j++) 
						{
							if(((VORoute) listaRutas.getElement(j)).id().equals(rutaID))
							{
								ruta = ((VORoute) listaRutas.getElement(j));
								encontro = true;
							}
						}


						rutasPorParada.addLast(ruta);

						laLista.addFirst(viajesPorParada);

					}
				}
			}

			stopsUtilizados.addFirst(laLista);
		}

		return stopsUtilizados;
	}


	@Override
	public RingList<VORoute> ITSrutasPorEmpresaParadas(String nombreEmpresa, String fecha) 
	{
		RingList rta = new RingList<VORoute>();
		RingList rtaReal = new RingList<VORoute>();
		RingList serviceIDs = new RingList<String>();
		RingList routesIDsFecha = new RingList<String>();
		for (int i = 0; i < listaCalendario.size(); i++) 
		{
			if(((VOCalendar) listaCalendario.getNode(i).getElement()).start_date().equals(fecha))
			{
				serviceIDs.addFirst(((VOCalendar)listaCalendario.getNode(i).getElement()).service_id());
			}
		}
		for (int i = 0; i < listaViajes.size(); i++) 
		{

			for (int j = 0; j < serviceIDs.size(); j++)
			{
				String serviceidtrip = ((VOTrip)listaViajes.getNode(i).getElement()).service_id();
				String serviceIDS = ((String)serviceIDs.getNode(j).getElement());
				String routeID = ((VOTrip)listaViajes.getNode(i).getElement()).route_id();
				if(serviceIDs.equals(serviceidtrip))
				{
					routesIDsFecha.addFirst(routeID);
				}
			}
		}
		for (int i = 0; i < listaRutas.size(); i++) 
		{
			for (int j = 0; j < routesIDsFecha.size(); j++)
			{
				String rutaIDrutas = ((VORoute)listaRutas.getNode(i).getElement()).id();
				String rutaFecha = ((String)routesIDsFecha.getNode(j).getElement());
				if(rutaIDrutas.equals(rutaFecha))
				{
					rta.addFirst(((VORoute)listaRutas.getNode(i).getElement()));
				}


			}
		}
		for (int i = 0; i < rta.size(); i++)
		{
			VORoute ruta = (VORoute) rta.getElement(i);
			if(ruta.agencia_id().equals(nombreEmpresa))
			{
				rtaReal.addFirst(ruta);
			}
		}

		for (int i = rtaReal.size(); i >= 0; i--)
		{
			for (int j = 0 ; j < rta.size() -1; j++)
			{
				VORoute ruta1 = ((VORoute)rtaReal.getNode(j).getElement());
				VORoute ruta2 = ((VORoute)rtaReal.getNode(j+1).getElement());
				if(ruta1.numParadas() > ruta2.numParadas())
				{
					rtaReal.switchWithNext(j);
				}
			}
		}

		return rtaReal;

	}



	public RingList<VOTrip> ITSviajesRetrasoTotalRuta2(String idRuta, String fecha)
	{
		RingList rta = new RingList<VOTrip>();
		RingList serviceIDsfecha = new RingList<String>();
		RingList viajesConRetardo = new RingList<VOTrip>();
		for (int i = 0; i < listaCalendario.size(); i++) 
		{
			if(((VOCalendar) listaCalendario.getNode(i).getElement()).start_date().equals(fecha))
			{
				serviceIDsfecha.addFirst(((VOCalendar)listaCalendario.getNode(i).getElement()).service_id());
			}
		}
		for (int i = 0; i < listaViajes.size(); i++)
		{
			for (int j = 0; j < serviceIDsfecha.size(); j++)
			{
				String rutaIDviajes = ((VOTrip)listaViajes.getNode(i).getElement()).route_id();
				String serviceIDviajes = ((VOTrip)listaViajes.getNode(i).getElement()).service_id();
				if(rutaIDviajes.equals(idRuta) && serviceIDviajes.equals((((String)serviceIDsfecha.getNode(j).getElement()))))
				{
					rta.addFirst(listaViajes.getNode(i).getElement());
				}
			}
		}

		for (int i = 0; i < rta.size(); i++)
		{

			for (int j = 0; j < listaRetrasos.size(); j++)
			{
				VORetraso retraso = (VORetraso) listaRetrasos.getNode(j).getElement();
				VOTrip viaje = (VOTrip)rta.getNode(i).getElement();
				if(retraso.viaje().equals(viaje.trip_id()) && viajesConRetardo.buscar(viaje) == null)
				{
					viajesConRetardo.addFirst(viaje);
				}
			}
		}
		for (int i = viajesConRetardo.size(); i >= 0; i--)
		{
			for (int j = 0 ; j < viajesConRetardo.size() -1; j++)
			{
				VORoute viajei = ((VORoute)viajesConRetardo.getNode(j).getElement());
				VORoute viajej = ((VORoute)viajesConRetardo.getNode(j+1).getElement());
				if(viajei.id().compareTo(viajej.id()) >0)
				{
					rta.switchWithNext(j);
				}
			}
		}

		return viajesConRetardo;

	}
	@Override
	public RingList<VOTrip> ITSviajesRetrasoTotalRuta(String idRuta, String fecha)
	{
		RingList viajesConRetardo = new RingList<VOTrip>();

		for (int i = 0; i < listaRetrasos.size(); i++)
		{
			String fechaRetraso = ((VORetraso)listaRetrasos.getElement(i)).darFecha();
			String idRutaRetraso =  ((VORetraso)listaRetrasos.getElement(i)).ruta().id();
			if(fechaRetraso.equals(fecha) && idRutaRetraso.equals(idRuta) )
			{
				viajesConRetardo.addFirst(listaRetrasos.getElement(i));
			}

		}

		for (int i = 0; i < viajesConRetardo.size(); i++)
		{

			for (int j = 0; j < ((VOTrip)viajesConRetardo.getNode(i).getElement()).darParadas().size() ; j++)
			{
				VOStop stop = ((VOStop)((VOTrip)viajesConRetardo.getNode(i).getElement()).darParadas().getElement(j));
				if(stop.huboRetraso())
				{		
					for (int j2 = j; j2 < ((VOTrip)viajesConRetardo.getNode(i).getElement()).darParadas().size(); j2++) 
					{
						VOStop stopj2 = ((VOStop)((VOTrip)viajesConRetardo.getNode(i).getElement()).darParadas().getElement(j2));
						if(!stopj2.huboRetraso())
						{
							viajesConRetardo.removePos(i);
							break;
						}

					}
				}			
			}
		}
		for (int i = viajesConRetardo.size(); i >= 0; i--)
		{
			for (int j = 0 ; j < viajesConRetardo.size() -1; j++)
			{
				VORoute viajei = ((VORoute)viajesConRetardo.getNode(j).getElement());
				VORoute viajej = ((VORoute)viajesConRetardo.getNode(j+1).getElement());
				if(viajei.id().compareTo(viajej.id()) >0)
				{
					viajesConRetardo.switchWithNext(j);
				}
			}
		}

		return viajesConRetardo;

	}


	@Override
	public RingList<RangoHora> ITSretardoHoraRuta(String idRuta, String Fecha) {

		RingList viajesConRetardo = new RingList<VOTrip>();
		RingList rangos = new RingList<RangoHora>();

		for (int i = 0; i < listaRetrasos.size(); i++)
		{
			String fechaRetraso = ((VORetraso)listaRetrasos.getElement(i)).darFecha();
			String idRutaRetraso =  ((VORetraso)listaRetrasos.getElement(i)).ruta().id();
			if(fechaRetraso.equals(Fecha) && idRutaRetraso.equals(idRuta) )
			{
				viajesConRetardo.addFirst(listaRetrasos.getElement(i));
			}

		}

		for (int i = 0; i < viajesConRetardo.size(); i++) 
		{
			RangoHora rangoi = ((VORetraso)viajesConRetardo.getElement(i)).darRango();
			rangos.addFirst(rangoi);

		}

		for (int i = rangos.size(); i >= 0; i--)
		{
			for (int j = 0 ; j < rangos.size() -1; j++)
			{
				RangoHora rangoj = ((RangoHora)rangos.getElement(j));
				RangoHora rangoj2 = ((RangoHora)rangos.getElement(j+1));;
				if(rangoj.darRetrasos().size()>rangoj2.darRetrasos().size())
				{
					rangos.switchWithNext(j);
				}
			}
		}



		return rangos;
	}





	@Override
	public RingList<VOTrip> ITSbuscarViajesParadas(String idOrigen, String idDestino, String fecha, String horaInicio,
			String horaFin) {

		RingList<VOTrip> viajes = new RingList<VOTrip>(); 
		RingList<VOTrip> viajesrta = new RingList<VOTrip>(); 
		RingList serviceIDs = new RingList<String>();
		for (int i = 0; i < listaCalendario.size(); i++) 
		{
			if(((VOCalendar) listaCalendario.getNode(i).getElement()).start_date().equals(fecha))
			{
				serviceIDs.addFirst(((VOCalendar)listaCalendario.getNode(i).getElement()).service_id());
			}
		}
		for (int i = 0; i < listaViajes.size(); i++) 
		{

			for (int j = 0; j < serviceIDs.size(); j++)
			{
				String serviceidtrip = ((VOTrip)listaViajes.getNode(i).getElement()).service_id();
				String serviceIDS = ((String)serviceIDs.getNode(j).getElement());
				if(serviceIDs.equals(serviceidtrip))
				{
					viajes.addFirst(((VOTrip)listaViajes.getNode(i).getElement()));
				}
			}
		}

		for (int i = 0; i < viajes.size(); i++)
		{
			for (int j = 0; j < ((VOTrip)viajes.getElement(i)).numParadas(); j++) 
			{
				boolean sirve = false;
				boolean origen = false;
				if(((VOStop)((VOTrip)viajes.getElement(i)).darParadas().getElement(j)).id().equals(idOrigen))
				{
					origen = true;
				}
				if(origen == true && ((VOStop)((VOTrip)viajes.getElement(i)).darParadas().getElement(j)).id().equals(idDestino))
				{
					sirve = true;
				}

				if(!sirve)
				{
					viajes.removePos(i);
				}
			}
		}
		RingList<String> idTrips = new RingList<String>();

		while(actualizacionBus.iterator().hasNext())
		{
			VOBusUpdate cosa = (VOBusUpdate) actualizacionBus.iterator().next();
			if(cosa.horaAsegundos() >= horaAsegundos(horaInicio) && cosa.horaAsegundos() <= horaAsegundos(horaFin))
			{
				idTrips.addFirst(cosa.trip_id());
			}
		}

		for (int i = 0; i < viajes.size(); i++)
		{
			for (int j = 0; j < idTrips.size(); j++) 
			{
				if(((VOTrip)viajes.getElement(i)).equals(idTrips))
				{
					viajesrta.addFirst((VOTrip) viajes.getElement(i));
				}

			}
		}

		return viajesrta;
	}


	@Override
	public VORoute ITSrutaMenorRetardo(String idRuta, String fecha) 
	{
		VORoute ruta = null;
		RingList rta = new RingList<VORoute>();
		RingList serviceIDs = new RingList<String>();
		RingList routesIDsFecha = new RingList<String>();
		for (int i = 0; i < listaCalendario.size(); i++) 
		{
			if(((VOCalendar) listaCalendario.getNode(i).getElement()).start_date().equals(fecha))
			{
				serviceIDs.addFirst(((VOCalendar)listaCalendario.getNode(i).getElement()).service_id());
			}
		}
		for (int i = 0; i < listaViajes.size(); i++) 
		{

			for (int j = 0; j < serviceIDs.size(); j++)
			{
				String serviceidtrip = ((VOTrip)listaViajes.getNode(i).getElement()).service_id();
				String serviceIDS = ((String)serviceIDs.getNode(j).getElement());
				String routeID = ((VOTrip)listaViajes.getNode(i).getElement()).route_id();
				if(serviceIDs.equals(serviceidtrip))
				{
					routesIDsFecha.addFirst(routeID);
				}
			}
		}
		for (int i = 0; i < listaRutas.size(); i++) 
		{
			for (int j = 0; j < routesIDsFecha.size(); j++)
			{
				String rutaIDrutas = ((VORoute)listaRutas.getNode(i).getElement()).id();
				String rutaFecha = ((String)routesIDsFecha.getNode(j).getElement());
				if(rutaIDrutas.equals(rutaFecha))
				{
					rta.addFirst(((VORoute)listaRutas.getNode(i).getElement()));
				}


			}
		}

		RingList rutasConElID = new RingList<VORoute>();
		for (int i = 0; i < rta.size(); i++)
		{
			VORoute ruta1 = (VORoute) rta.getElement(i);
			if(ruta1.id().equals(idRuta))
			{
				rutasConElID.addFirst(ruta1);
			}
		}

		ruta= (VORoute) rutasConElID.getElement(0);
		double promedioMax = ruta.darPromedioRetrasosSegundos();
		for (int i = 0; i < rutasConElID.size(); i++) 
		{
			if(((VORoute)rutasConElID.getElement(i)).darPromedioRetrasosSegundos() > promedioMax)
			{
				ruta = (VORoute) rutasConElID.getElement(i);
				promedioMax = ((VORoute) rutasConElID.getElement(i)).darPromedioRetrasosSegundos();
			}
		}

		return ruta;
	}


	@Override
	public void ITSInit() 
	{
		ITScargarGTFS();
		ITScargarTR();
		cargarRetrasos();

	}


	@Override
	public RingList<VOservice> ITSserviciosMayorDistancia(String fecha)
	{
		RingList<VOservice> lista = new RingList<VOservice>();

		for (int i = listaServices.size(); i >= 0; i--)
		{
			for (int j = 0 ; j < listaServices.size() -1; j++)
			{
				VOservice service1 = ((VOservice)listaServices.getElement(j));
				VOservice service2 = ((VOservice)listaServices.getElement(j+1));;
				if(service1.distanciasSumadas()>service2.distanciasSumadas())
				{
					listaServices.switchWithNext(j);
				}
			}
		}

		return lista;
	}


	@Override
	public RingList<VORetraso> ITSretardosViaje(String fecha, String idViaje) 
	{
		RingList<VORetraso> retrasos = new RingList<VORetraso>();

		for (int i = 0; i < listaRetrasos.size(); i++) 
		{
			if(((VORetraso) listaRetrasos.getElement(i)).darFecha().equals(fecha) && ((VORetraso) listaRetrasos.getElement(i)).viaje().trip_id().equals(idViaje))
			{
				retrasos.addFirst((VORetraso) listaRetrasos.getElement(i));
			}
		}

		return retrasos;
	}


	@Override
	public RingList<VOStop> ITSparadasCompartidas(String fecha) 
	{
		RingList<VOStop> paradas = new RingList<VOStop>();

		for (int i = 0; i < listaParadas.size(); i++) 
		{
			if (((VOStop) listaParadas.getElement(i)).darRutas().size() > 1)
			{
				paradas.addFirst((VOStop) listaParadas.getElement(i));
			}
		}

		return paradas;
	}

	public static void main(String[] args) 
	{
		STSManager variable = new STSManager();
		
		String idParadaOrigen = "";
		String idParadaDestino = "";
		String fecha = "";
		String horaInicio = "";
		String horaFin = "";
		
		RingList<VOTrip> lista = variable.ITSbuscarViajesParadas(idParadaOrigen, idParadaDestino, fecha, horaInicio, horaFin);
		
		for (int i = 0; i < lista.size(); i++) 
		{
					System.out.println(((VOTrip) lista.getElement(i)).trip_id());

		}
		
	}

}
