package model.vo;


public class VOBusUpdate 
{

	private String vehicleNo;
	private String trip_id;
	private String routeNo;
	private String direction;
	private String destination;
	private String pattern;
	private String latitude;
	private String longitude;
	private String recordedTime;
	private String routeMap;
	
	
	public VOBusUpdate(String pVehicleNo, String pTrip_id, String pRouteNo, String pDirection, String pDestination, String pPattern, String pLatitude, String pLongitude, String pRecordedTime, String  pRouteMap )
	{
		vehicleNo = pVehicleNo;
		trip_id = pTrip_id;
		routeNo = pRouteNo;
		direction = pDirection;
		destination = pDestination;
		pattern = pPattern;
		latitude = pLatitude;
		longitude = pLongitude;
		recordedTime = pRecordedTime;
		routeMap = pRouteMap;
	}

	
	/**
	 * @return id - vehicule's number
	 */
	public String vehicleNo() 
	{
		return vehicleNo;
	}
	
	public String trip_id() 
	{
		return trip_id;
	}
	
	public String routeNo() 
	{
		return routeNo;
	}
	
	public String direction() 
	{
		return direction;
	}
	
	public String destination() 
	{
		return destination;
	}
	
	public String pattern() 
	{
		return pattern;
	}
	
	public String latitude() 
	{
		return latitude;
	}
	
	public String longitude() 
	{
		return longitude;
	}
	
	public String recordedTime() 
	{
		return recordedTime;
	}
	
	public String routeMap() 
	{
		return routeMap;
	}
	
	public int horaAsegundos()
	{
		int rta = 0;
		String[] arreglo= recordedTime.split(":");
		String[] segundosAMPM = arreglo[2].split(" ");
		rta += ((3600* Integer.parseInt(arreglo[0]))+(60*Integer.parseInt(arreglo[1]))+(Integer.parseInt(segundosAMPM[0])));
		if(segundosAMPM[1].equalsIgnoreCase("pm"))
		{
			rta += (12*3600);
		}
		
		return rta;
	}
	
}
