package model.vo;

import model.data_structures.DoubleLinkedList;

public class VOCalendar 
{
	private String service_id;

	private String monday;

	private String tuesday;

	private String wednesday;

	private String thursday;

	private String friday;

	private String saturday;

	private String sunday;

	private String start_date;

	private String end_date;
	
	private DoubleLinkedList<VOCalendarDates> excepciones;

	public VOCalendar (String pService_id, String pMonday, String pTuesday, String pWednesday, String pThursday, String pFriday, String pSaturday, String pSunday, String pStart_date, String pEnd_date)
	{
		service_id = pService_id;
		monday = pMonday;
		tuesday = pTuesday;
		wednesday = pWednesday;
		thursday = pThursday;
		friday = pFriday;
		saturday = pSaturday;
		sunday = pSunday;
		start_date = pStart_date;
		end_date = pEnd_date;
		
	}

	public String service_id()
	{
		return service_id;
	}
	
	public String monday()
	{
		return monday;
	}
	
	public String tuesday()
	{
		return tuesday;
	}
	
	public String wednesday()
	{
		return wednesday;
	}
	
	public String thursday()
	{
		return thursday;
	}
	
	public String friday()
	{
		return friday;
	}
	
	public String saturday()
	{
		return saturday;
	}
	
	public String sunday()
	{
		return sunday;
	}
	
	public String start_date()
	{
		return start_date;
	}
	
	public String end_date()
	{
		return end_date;
	}
}
