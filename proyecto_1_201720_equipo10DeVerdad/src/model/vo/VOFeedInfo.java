package model.vo;

public class VOFeedInfo 
{
	private String feed_publisher_name;
	
	private String feed_publisher_url;
	
	private String feed_lang;

	private String feed_start_date;
	
	private String feed_end_date;

	private String feed_version;
	
	public VOFeedInfo(String pfeed_publisher_name, String pfeed_publisher_url, String pfeed_lang, String pfeed_start_date, String pfeed_end_date, String pfeed_version )
	{
		feed_publisher_name = pfeed_publisher_name;
		feed_publisher_url = pfeed_publisher_url;
		feed_lang = pfeed_lang;
		feed_start_date = pfeed_start_date;
		feed_end_date = pfeed_end_date;
		feed_version = pfeed_version;
		
	}
	
	public String feed_publisher_name()
	{
		return feed_publisher_name;
	}
	
	public String feed_publisher_url()
	{
		return feed_publisher_url;
	}
	
	public String feed_lang()
	{
		return feed_lang;
	}
	
	public String feed_start_date()
	{
		return feed_start_date;
	}
	
	public String feed_end_date()
	{
		return feed_end_date;
	}
	
	public String feed_version()
	{
		return feed_version;
	}
}
