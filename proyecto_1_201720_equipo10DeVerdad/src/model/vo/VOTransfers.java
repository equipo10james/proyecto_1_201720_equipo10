package model.vo;

public class VOTransfers 
{
 
	private String from_stop_id;
	private String to_stop_id;
	private String transfer_type;
	private String min_transfer_time;
	
	public VOTransfers(String pfrom_stop_id, String pto_stop_id, String ptransfer_type, String pmin_transfer_time)
	{
		from_stop_id = pfrom_stop_id;
		to_stop_id = pto_stop_id;
		transfer_type = ptransfer_type;
		min_transfer_time = pmin_transfer_time;
	}
	
	public String from_stop_id()
	{
		return from_stop_id;
	}
	
	public String to_stop_id()
	{
		return to_stop_id;
	}
	
	public String transfer_type()
	{
		return transfer_type;
	}
	
	public String min_transfer_time()
	{
		return min_transfer_time;
	}
}
