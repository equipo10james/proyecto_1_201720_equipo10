package model.vo;

import model.data_structures.RingList;

public class VOZone 
{
	private String zone_id;
	private RingList<VOStop> listaParadas;

	public VOZone(String pZone_id, VOStop parada )
	{
		zone_id = pZone_id;
		listaParadas.addFirst(parada); 

		ordenarParadas();
	}

	public void agregarParada(VOStop parada)
	{
		listaParadas.addFirst(parada); 

		ordenarParadas();
	}
	
	public String zone_id()
	{
		return zone_id;
	}

	public void ordenarParadas()
	{
		for (int i = 1; i < listaParadas.size(); i++) 
		{
			for (int j = 0; j < listaParadas.size()-1; j++) 
			{
				if(((VOStop) listaParadas.getElement(j)).id().compareTo(((VOStop) listaParadas.getElement(j+1)).id()) > 0)
				{
					listaParadas.switchWithNext(j);
				}
			}
		}
	}
}
