package model.vo;

import model.data_structures.RingList;

public class VOservice 
{

	private String service_id;
	private RingList<VOTrip> trips;
	
	
	public VOservice(String id)
	{
		service_id = id;
	}
	
	public String darService_id()
	{
		return service_id;
	}
	public void agregarTrip(VOTrip trip)
	{
		trips.addFirst(trip);
	}
	
	public double distanciasSumadas()
	{
		double rta = 0;
		for (int i = 0; i < trips.size(); i++) 
		{
			rta += ((VOTrip)trips.getElement(i)).distancia();
		}
		
		
		return rta;
	}
}
